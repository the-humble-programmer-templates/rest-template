using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using RESTApplication.Beverage.Repositories;
using RESTApplication.Beverage.Services;

namespace RESTApplication
{
     public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        // Defining a static singleton object
        // In theory this is unnecessary, but I have had strange experiences with singletons when not using this
        private static ISampleRepository _repo = new SampleRepository();
        
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add MVC controllers, ASP.NET will magically find them based on the Attribute [ApiController]
            services.AddControllers().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            // There are 3 types of additional 'services' that can be added
            // Transient objects are created every time they are called and should be lightweight
            // Scoped objects are created on every request that needs them
            // Singletons persist across all requests
            // Upon starting the application, ASP.NET will use these services and controllers to perform Dependency Injection mapping
            // This automatically creates the object instances and feeds in the relevant dependencies as needed.
            services.AddScoped<ISampleService, SampleService>();
            services.AddSingleton<ISampleRepository>(_repo);
            // Generate Swagger API Documentation
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Sample API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            
            app.UseRouting();
            // Tells the app when starting up to use Swagger
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "MyAPI V1");
            });
            // Tells the app to generate the controller endpoints
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}