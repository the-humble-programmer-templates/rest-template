using System;
using System.Collections.Generic;
using RESTApplication.Beverage.Models;

namespace RESTApplication.Beverage.Services
{
    public interface ISampleService
    {
        // Misc needed methods
        IEnumerable<string> GetOptions();
        
        // Standard CRUD methods
        Guid CreateObj(string name, SampleEnum type);
        IEnumerable<SampleResponseModel> GetObjects(string name);
        bool UpdateObject(Guid id, string customer, SampleEnum type);
        bool DeleteObject(Guid id, string customer);
    }
}