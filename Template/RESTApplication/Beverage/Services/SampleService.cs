using System;
using System.Collections.Generic;
using System.Linq;
using RESTApplication.Beverage.Models;
using RESTApplication.Beverage.Repositories;

namespace RESTApplication.Beverage.Services
{
    // Business Logic Layer class used to orchestrate entities
    public class SampleService: ISampleService
    {
        // Dependencies (From Domain Entity layer as per 3 tier architecture)
        private ISampleRepository _repo;

        // Constructor with dependency passed in for Dependency Injection
        public SampleService(ISampleRepository repo)
        {
            this._repo = repo;
        }

        // Implementation of method
        public IEnumerable<string> GetOptions()
        {
            return Enum.GetNames(typeof(SampleEnum));
        }

        // Implementation of CRUD
        public Guid CreateObj(string name, SampleEnum type)
        {
            // Performing some data preparation
            var time = DateTime.Now;
            var id = Guid.NewGuid();
            // Construct domain object via Tuple Deconstruction before passing to Domain Entity Layer
            _repo.CreateObj(new SampleDomainModel{Id = id, Customer = name, EnumType = type, Date = time});
            return id;
        }
        
        public IEnumerable<SampleResponseModel> GetObjects(string name)
        {
            // Obtain Entity Models
            var drinks = _repo.GetObjects(name);
            // Use LINQ to convert to Response Model for returning to caller
            var serving = drinks.Select(x => 
                new SampleResponseModel(x.Customer, x.EnumType.ToString(), Calculate(DateTime.Now - x.Date))
            );
            return serving;
        }
        
        // Update here is defined like in PUT requests, wehre the entire entry is replaced.
        public bool UpdateObject(Guid id, string customer, SampleEnum type)
        {
            // Do some computation
            var time = DateTime.Now;
            // Cancel existing 
            var cancelled = _repo.DeleteObj(id, customer, out SampleDomainModel model);
            
            if (!cancelled) return false;
            // Create a new drink to replace the old one
            _repo.CreateObj(new SampleDomainModel{Id = id, Customer = model.Customer, EnumType = type, Date = time});
            return true;
        }

        public bool DeleteObject(Guid id, string customer)
        {
            return _repo.DeleteObj(id, customer, out _);
        }



        // Private methods to do internal calculations
        private double Calculate(TimeSpan time)
        {
            var temp = 28 + 52 * Math.Pow(Math.E, -0.0462098 * time.TotalMinutes);
            return temp;
        }
    }
}