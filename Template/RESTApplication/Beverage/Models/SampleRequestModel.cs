namespace RESTApplication.Beverage.Models
{
    // Request model for incoming POST/PUT requests
    public class SampleRequestModel
    {
        // Properties
        public string Name { get; set; }
        public string Type { get; set; }
        
        // If there is no constructor, an empty constructor is compiler generated (in this case for JSON deserialisation)
    }
}