using System;

namespace RESTApplication.Beverage.Models
{
    // Domain Models are used internally in business logic for processing
    public class SampleDomainModel
    {
        // Properties Declaration
        public DateTime Date { get; set; }

        public string Customer { get; set; }
        
        public SampleEnum EnumType { get; set; }
        
        public Guid Id { get; set; }

        // Default value for properties can be defined
        public bool Checked { get; set; } = false;
        
        // Empty constructor needed for JSON Deserialisation (If relevant)
        public SampleDomainModel(){}

        // Constructor for easy construction
        public SampleDomainModel(DateTime date, string customer, SampleEnum enumType, Guid id)
        {
            Date = date;
            Customer = customer;
            EnumType = enumType;
            Id = id;
        }
    }
}