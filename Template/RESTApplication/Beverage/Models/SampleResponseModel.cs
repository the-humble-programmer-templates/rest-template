namespace RESTApplication.Beverage.Models
{
    // Response model to return to Clients (usually for GET requests)
    public class SampleResponseModel
    {
        // Properties to return
        public string Name { get; set; }
        public string Type { get; set; }
        public double Value { get; set; }
        
        // Empty constructor needed for JSON Deserialisation (If relevant)
        public SampleResponseModel(){}
        
        // Constructor for easy construction
        public SampleResponseModel(string name, string type, double value)
        {
            Name = name;
            Type = type;
            Value = value;
        }
    }
}