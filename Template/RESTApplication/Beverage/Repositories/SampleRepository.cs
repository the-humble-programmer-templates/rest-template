using System;
using System.Collections.Generic;
using System.Linq;
using RESTApplication.Beverage.Models;

namespace RESTApplication.Beverage.Repositories
{
    // Repositories deal with data management and may communicate with external databases etc to obtain entity data before preparing it as relevant domain models.
    public class SampleRepository: ISampleRepository
    {
        // Model Dictionary
        private Dictionary<string, List<SampleDomainModel>> _modelDict = new Dictionary<string, List<SampleDomainModel>>();
        
        public SampleRepository(){}
        public IEnumerable<SampleDomainModel> GetObjects(string name)
        {
            // Query via LINQ
            var models = _modelDict[name].Where(x => !x.Checked);
            
            // Do some computation to manage the models
            foreach (var model in models)
            {
                model.Checked = true;
            }
            return models;
        }

        public void CreateObj(SampleDomainModel model)
        {
            if(!_modelDict.ContainsKey(model.Customer)) _modelDict.Add(model.Customer, new List<SampleDomainModel>());
            _modelDict[model.Customer].Add(model);
        }

        // Out means the object reference passed in is empty, and will be set to some output of the type
        // Ref is an alternative to indicate that the reference passed in is not empty
        // This function attempts to find and delete a domain model object and returns the deleted object via the Out parameter.
        public bool DeleteObj(Guid id, string customer, out SampleDomainModel model)
        {
            model = null;
            if (!_modelDict.ContainsKey(customer)) return false;
            for(var i = 0; i < _modelDict[customer].Count; i++)
            {
                if (!_modelDict[customer][i].Checked && _modelDict[customer][i].Id == id)
                {
                    model = _modelDict[customer][i];
                    _modelDict[customer].RemoveAt(i);
                    return true;
                }
            }

            return false;
        }
    }
}