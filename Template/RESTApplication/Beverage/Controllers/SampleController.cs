﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using RESTApplication.Beverage.Models;
using RESTApplication.Beverage.Services;

namespace RESTApplication.Beverage.Controllers
{
    // Indicates that this is REST controller
    [ApiController]
    // Specifies the URL extension
    [Route("api/[controller]")]
    public class SampleController : ControllerBase
    {
        // Uses services as a dependency
        private ISampleService _service;

        // Constructor for Dependency Injection
        public SampleController(ISampleService service)
        {
            this._service = service;
        }
        
        // Function will describe a GET endpoint at <controller route>/options
        [HttpGet("options")]
        // ActionResult is a specific type of return allowing you to control headers and status codes
        public ActionResult<IEnumerable<string>> Options()
        {
            // Return status code 200 with message body
            return Ok(_service.GetOptions());
        }
        
        // Function describes a GET endpoint at <controller route>
        [HttpGet]
        public ActionResult<string> Get()
        {
            // Return custom status code
            return StatusCode(418, "This response is short and stout!");
        }
        
        // Custom route with parameters <controller route>/customer/{name param}
        [HttpGet("customer/{name}")]
        // Name is gotten from the {name} in the route, as indicated by [FromRoute]
        public ActionResult<IEnumerable<SampleDomainModel>> Get([FromRoute] string name)
        {
            var serving = _service.GetObjects(name);
            return Ok(serving);
        }

        // Post Endpoint at <controller route>
        [HttpPost]
        // Requires a SampleRequestModel in message body, passed in JSON format. ASP.NET automatically deserialises incoming input into the object
        public ActionResult<Guid> Brew([FromBody] SampleRequestModel model)
        {
            // Preliminary Input Validation
            var parsed = Enum.TryParse(model.Type, out SampleEnum type);
            if (!parsed)
            {
                return BadRequest("Invalid Type!");
            }
            // Call Service
            var id = _service.CreateObj(model.Name, type);
            return Ok(id);
        }
        
        // Put Endpoint at route <controller route>/order/{drinkID}
        [HttpPut("order/{drinkId}")]
        // Parameters from the route as well as the body
        public ActionResult<IEnumerable<SampleDomainModel>> Replace([FromRoute] Guid id, [FromBody] SampleRequestModel model)
        {
            var parsed = Enum.TryParse(model.Type, out SampleEnum type);
            if (!parsed)
            {
                return BadRequest("Invalid Drink Type!");
            }
            if(!_service.UpdateObject(id, model.Name, type)) return NotFound("ID not found");
            return Ok();
        }
        
        // Delete endpoint with route <controller route>/customer/{name}/order/{drinkId}
        [HttpDelete("customer/{name}/order/{drinkId}")]
        public ActionResult<IEnumerable<SampleDomainModel>> Cancel([FromRoute] Guid id, [FromRoute] string name)
        {
            if(!_service.DeleteObject(id, name)) return NotFound("ID not found");
            return NoContent();
        }
    }
}